import { InputType, Field } from 'type-graphql';
import { Length } from 'class-validator';

@InputType()
export class ChangePasswordInput {
  @Field()
  @Length(8, 60)
  password: string;

  @Field()
  token: string;
}
